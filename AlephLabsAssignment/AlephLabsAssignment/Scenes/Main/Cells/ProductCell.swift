//
//  ProductCell.swift
//  AlephLabsAssignment
//
//  Created by korstudio on 22/5/18.
//  Copyright © 2018 Methas Tariya. All rights reserved.
//

import UIKit
import Kingfisher

class ProductCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func displayInfo(product: Product) {
        imageView.kf.setImage(with: product.imageURL, placeholder: UIImage(color: UIColor.darkGray))
        titleLabel.text = product.title
        priceLabel.text = product.price
    }
}
