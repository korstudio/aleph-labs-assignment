//
//  Product.swift
//  AlephLabsAssignment
//
//  Created by korstudio on 22/5/18.
//  Copyright © 2018 Methas Tariya. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Product {
    var title: String = ""
    var price: String = ""
    var imageURL: URL!
    
    init(json: JSON) {
        title = json["title"].stringValue
        price = json["price"].stringValue
        
        let imageURLString = json["image"].stringValue
        imageURL = URL(string: imageURLString)
    }
}
