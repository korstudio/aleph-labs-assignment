//
//  BaseServiceExtension.swift
//  SmartRetail
//
//  Created by korstudio on 12/17/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import Foundation
import Moya

extension TargetType {
    var method: Moya.Method {
        return .get
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    
    static func create<T>() -> MoyaProvider<T> where T: TargetType {
        return MoyaProvider<T>()
    }
}

extension String {
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}

extension Data {
    var utf8String: String? {
        return String(data: self, encoding: .utf8)
    }
}
