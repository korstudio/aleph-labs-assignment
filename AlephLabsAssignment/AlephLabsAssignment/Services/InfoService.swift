//
//  InfoService.swift
//  AlephLabsAssignment
//
//  Created by korstudio on 9/5/18.
//  Copyright © 2018 Methas Tariya. All rights reserved.
//

import Foundation
import Moya

enum InfoService {
    case getAppleData
    case getAndroidData
}

extension InfoService: TargetType {
    var baseURL: URL {
        return URL(string: "http://dev.aleph-labs.com/ios_assignment")!
    }
    
    var path: String {
        switch self {
        case .getAppleData:
            return "/apple.json"
        case .getAndroidData:
            return "/android.json"
        }
    }
    
    var sampleData: Data {
        return "".utf8Encoded
    }
    
    var task: Task {
        return .requestPlain
    }
}

