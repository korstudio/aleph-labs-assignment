//
//  AlertDialog.swift
//  AlephLabsAssignment
//
//  Created by korstudio on 22/5/18.
//  Copyright © 2018 Methas Tariya. All rights reserved.
//

import Foundation
import UIKit

enum DialogLevel {
    case `default`
    case danger
}

class Dialog {
    
    class func show(on view: UIViewController, title: String, message: String, dismiss: (()->Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { (action) in
            if let callback = dismiss {
                callback()
            }
            alert.dismiss(animated: true, completion: nil)
        }))
        view.present(alert, animated: true)
    }
    
    class func confirm(on view: UIViewController, title: String, message: String, yesTitle: String, noTitle: String, level: DialogLevel = .default, yes: (()->Void)? = nil, no: (()->Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let yesActionStyle: UIAlertActionStyle = (level == .danger ? .destructive : .default)
        alert.addAction(UIAlertAction(title: yesTitle, style: yesActionStyle, handler: { (alertAction) in
            if let callback = yes {
                callback()
            }
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: noTitle, style: .cancel, handler: { (alertAction) in
            if let callback = no {
                callback()
            }
            alert.dismiss(animated: true, completion: nil)
        }))
        
        view.present(alert, animated: true)
    }
}
