//
//  TabWithIndicatorControl.swift
//  AlephLabsAssignment
//
//  Created by korstudio on 12/7/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import UIKit

class TabWithIndicatorControl: UISegmentedControl {
    @IBInspectable var indicatorColor: UIColor = UIColor.white {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var indicatorSize: CGFloat = 2.0 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var titleNormalColor: UIColor = UIColor.black {
        didSet {
            updateTitleStyle()
        }
    }
    
    @IBInspectable var titleSelectedColor: UIColor = UIColor.white {
        didSet {
            updateTitleStyle()
        }
    }
    
    @IBInspectable var backgroundNormalColor: UIColor = UIColor.white {
        didSet {
            drawBackground()
        }
    }
    
    @IBInspectable var backgroundSelectedColor: UIColor = UIColor.black {
        didSet {
            drawBackground()
        }
    }
    
    //    required init?(coder aDecoder: NSCoder) {
    //        super.init(coder: aDecoder)
    //
    //        setup()
    //    }
    
    override func draw(_ rect: CGRect) {
        drawBackground()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    func setup() {
        updateTitleStyle()
        drawBackground()
        
        self.layer.cornerRadius = 0.0
        self.setDividerImage(UIImage(color: UIColor.clear), forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        self.setDividerImage(UIImage(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .selected, barMetrics: .default)
    }
    
    func updateTitleStyle() {
        let avenirFont = UIFont(name: "AvenirNext-Bold", size: 17)!
        self.setTitleTextAttributes([
            NSAttributedStringKey.foregroundColor: titleNormalColor,
            NSAttributedStringKey.font: avenirFont
            ], for: .normal)
        self.setTitleTextAttributes([
            NSAttributedStringKey.foregroundColor: tintColor,
            NSAttributedStringKey.font: avenirFont
            ], for: .selected)
    }
    
    func drawBackground() {
        self.backgroundColor = backgroundNormalColor
        self.setBackgroundImage(UIImage(color: UIColor.clear), for: .normal, barMetrics: .default)
        self.setBackgroundImage(drawIndicator(), for: .selected, barMetrics: .default)
    }
    
    func drawIndicator() -> UIImage? {
        let baseRect = CGRect(x: 0, y: 0, width: 1, height: self.frame.height)
        let indicatorRect = CGRect(x: 0, y: self.frame.height - indicatorSize, width: 1, height: indicatorSize)
        
        UIGraphicsBeginImageContext(baseRect.size)
        let context = UIGraphicsGetCurrentContext()
        
        //draw background color
        context?.setFillColor(backgroundSelectedColor.cgColor)
        context?.fill(baseRect)
        //draw indicator
        context!.setFillColor(indicatorColor.cgColor)
        context!.fill(indicatorRect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
