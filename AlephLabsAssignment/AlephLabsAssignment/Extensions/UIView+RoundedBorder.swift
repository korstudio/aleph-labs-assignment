//
//  UIVIew+RoundedStroke.swift
//  SmartRetail
//
//  Created by korstudio on 12/8/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    open func circularBorder() {
        setRoundedBorder(borderWidth: 1.0, color: .clear, cornerRadius: self.frame.width * 0.5)
    }
    
    open func setRoundedBorder(borderWidth: CGFloat = 1.0, color: UIColor = UIColor.clear, cornerRadius: CGFloat = 0.0) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        
        self.clipsToBounds = true
    }
}
