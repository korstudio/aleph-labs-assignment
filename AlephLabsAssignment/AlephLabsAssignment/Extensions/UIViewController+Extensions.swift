//
//  UIViewController+Extensions.swift
//  SmartRetail
//
//  Created by korstudio on 12/20/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

extension UIViewController {
    @objc func showProgress(label:String? = nil) {
        HUD.show(.labeledProgress(title: nil, subtitle: label))
    }
    
    @objc func hideProgress() {
        HUD.hide()
    }
}

extension UIView {
    func removeAllChildren() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
}
