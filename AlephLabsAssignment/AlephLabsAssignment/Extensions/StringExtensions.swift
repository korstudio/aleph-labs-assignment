//
//  StringExtensions.swift
//  SmartRetail
//
//  Created by korstudio on 1/4/18.
//  Copyright © 2018 Methas Tariya. All rights reserved.
//

import Foundation

extension String {
    static func formatNumber(_ number: NSNumber) -> String {
        let formatter = NumberFormatter()
        formatter.decimalSeparator = ","
        formatter.numberStyle = .decimal
        return formatter.string(from: number) ?? ""
    }
    
    var isValidEmail: Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    var isValidMobile: Bool {
        let mobileRegex = "0(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{1,7}$"
        return NSPredicate(format: "SELF MATCHES %@", mobileRegex).evaluate(with: self)
    }
}
