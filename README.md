# Assignment for Aleph-Labs
https://bitbucket.org/korstudio/aleph-labs-assignment

## Code Architecture

### Clean Swift
This project uses Clean Swift structure which can be referenced at:
https://clean-swift.com

Architecture detail:
https://clean-swift.com/clean-swift-ios-architecture/

## Installation

### First-time Installation
After clone the repo

1. Open Terminal or iTerm 2

2. Navigate to the recently cloned folder then open the `AlephLabsAssignment` folder

3. Type:
    `pod install`
    
4. After pods finished, open the project using `AlephLabsAssignment.xcworkspace`